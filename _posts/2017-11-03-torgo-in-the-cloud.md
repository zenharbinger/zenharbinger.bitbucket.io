---
layout: post
title: Torgo in the cloud
date: '2017-11-03 12:07:17 -0500'
categories: running
tags: [dev, java, grails]
author: Matthew Aguirre
---

Decided that I needed to see how easy it is to get an app to deploy to the `cloud`.  Using some on-line [docs](http://guides.grails.org/grails-google-cloud/guide/index.html) I was able to setup the [Grails port of Torgo](https://github.com/ZenHarbinger/torgo-grails) to run on [Google Cloud Platform (GCP)](https://github.com/ZenHarbinger/torgo-grails/tree/gcp).  I have removed the database root password, but the rest of the source is as running in the cloud.

It was touch and go, but I wasn't warned that the actual final 'Updating service' can take a LONG TIME.  I wasn't sure it was going to work at all, I kept getting `500` errors.

Eventually, it just finished (something like the 8'th attempt).

So without further ado.

# [Torgo Online!!](https://torgo.tros.org)
