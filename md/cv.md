My interests focus on creating solid, flexible software; reusable components; artificial intelligence; algorithm development; and solving problems in novel ways.

------------------

## SKILL SET (Alphabetical Order)
Android, ASP.NET, C#, C/C++, Computer Graphics, Database Management, Design Patterns, Gephi, Git, Grails, Groovy, Human-Computer Interaction, Java, JavaScript, Jenkins CI, Linux, Machine Translation (MT), OpenMQ, PostgresSQL, Protocol Buffer, Python, Software Agents, Subversion, Tomcat, Web-Services and Applications, WebSockets, ZeroMQ

------------------

## EXPERIENCE

#### Open Source Developer, Bristow, Virginia -- Software Engineer

***2015 - PRESENT***

- Enhance [self-service-password][56]; tag password fields for [best practice][57].
- Maintain [grails-jayspt][53].
- Contribute to [grails-jayspt][53]; updated for enctyption support in [Grails 4.0.0][52].
- Develop [Torgo](https://torgo.tros.org/) in Grails deployed via Google Cloud Platform.
- Contribute [[1][8], [2][9], [3][32]] updates for [plugins and test scripts][7] to [snapcraft][6].
- Develop [Torgo][10]; a Logo programming language interpreter written in Java for Linux, Windows, and macOS.
- Develop [Torgo-JavaScript][33]; a port of Torgo to JavaScript to run in a web-browser.
- Maintain [l2fprod Properties Editor][11]; a Swing GUI component that will allow modifying Java Bean properties in an extensible editor.
- Contribute to [Apache Commons Lang][12]; Added functionality to [check for duplicate event listeners][13].
- Contribute to [TUIO][30]; Submitted pull request to serialize [TUIO Java objects][31].  The request was merged manually.
- Develop [SpotiPy][14]; a Linux/Ubuntu music player written in Python for playing Spotify and local files utilizing GStreamer.

#### Einstein Technologies, Inc., Arlington, Virginia -- Senior Software Engineer

***2018 - Present***

- Responsibilities include IT and internal project development.
- Mentor for Interns.
- Representative Project Experience:
    - Setup OpenLDAP with single login credentials for Bitbucket, Confluence, Jira, OpenVPN, Samba, and laptop logins for CentOS/RHEL, Ubuntu, and Windows.  Includes provisions for on-line maintenance via [LDAP Account Manager][41] and [Self Service Password][42].
    - Use of [Splunk][43] for syslog monitoring.
    - Use of [Puppet][44] for configuration management.

#### ArtisTech, Inc., Centreville, Virginia -- Software Engineer

***2002 - 2018***

- Responsibilities include software design, implementation, documentation, and testing; server maintenance for SCM, continuous integration, and web; and communication with project members for integration and testing.
- Representative Project Experience:
    - Connected multiple Combat Net Radio (CNR) applications together via [EMANE][34] for Virtual Battlespace 3 ([VBS3][28]). (Java)
    - Created web-application for viewing tweet datasets for the [NSCTA][3]. ([SMS][35], Grails)
    - Developed a Grails application launcher to allow stand-alone Grails applications to be deployed easily on end-user's computers. (Grails)
    - Develop pipelines for extracting entities data from text. ([Base][36]: [Blue][38], [Orange][39], [Yellow][37], [Green][40]; Java)
    - Worked on DARPA ['Improv'][29] studying possible social media amplification.
    - Generated web-based front-end to CNF/OntoFlex. (Grails)
    - Implemented Visualization Framework at the Army Research Lab ([ARL][5]) utilizing [ZeroMQ][26], [Google Protocol Buffers][27], and [TUIO][4] with multi-touch devices. Utilize existing MultiTaction display devices to provide user interaction capabilities by providing mouse emulation functionality. ([Source][23]; Java)
    - Wrote a plug-in to Virtual Battlespace 3 ([VBS3][28]) to broadcast user position data for tracking. ([Source][22]; C++, Java, WebSockets, [D3][24])
    - Implemented an application for simulating social network/organizational behaviors. Used by the Network Science Collaborative Technology Alliance ([NSCTA][3]), this program acts as an integration point for testing simulated communication, information, and varying social network models. Also used for testing cloud-based, virtual server throughput by running as a distributed client-server application utilizing [OpenMQ][17]. ([AlgoLink][19]; Java)
    - Developed framework for processing 500 GB of scanned documents for translation and data extraction. The framework included "cleaning" the image with custom software, segmenting text out of the images with multiple segmenting components, OCR images, and translation of text. (C#)
    - Utilized [Kepler][16] application for showing the pipe-line work-flow for generating custom, statistical machine translation engines and automated post editors.
    - Created an algorithm for aligning source and reference segments for statistical MT training. Used in "[Boosting Performance of Weak MT Engines Automatically: Using MT Output to Align Segments & Build Statistical Post Editors][1]," the algorithm was used to generate high-quality parallel source corpora for low-resource languages in training statistical machine translation engines. (C#)
    - Maintained outpost computer network for hosting both custom and purchased software for the Army Research Lab ([ARL][5]) for machine translation research and evaluation. (Windows and Linux)
    - Wrote a web-scraper service for gathering foreign language news documents from specified sources. (C#)
    - Designed and implemented an ASP.NET web-application that allowed users to translate and evaluate foreign language documents using multiple machine translation engines and automated evaluation metrics. ([TIE][20]; C#)
    - Built web-service wrappers around machine translation services and applications, OCR applications, and image segmenting applications. ([TIE][20]; C#, C++, Perl, Python)
    - Enhanced an intelligent agent architecture utilizing enterprise level messaging ([OpenMQ][17]). Agents used to route-plan, monitor services, and update display on ArcMap. Increased throughput and reliability of agent communications. ([DARE][25]; C#, Java)
    - Developed a .NET and Java application for foreign language document triage/search using multilingual mind-maps, which also allowed users to create, visualize, and edit multilingual mind-maps. Using ontologies, a graph of domain specific knowledge can be built by a user for specifying keyword queries in a native language for searching documents in a foreign language. ([CNF][21]/OntoFlex; C#, Java)
    - Designed the architecture and implemented an ASP.NET web-application for task-based machine translation evaluation for the Center for Advanced Study of Language. Utilized in gathering the data for the paper "[Task-based MT Evaluation: From Who/When/Where Extraction to Event Understanding][2]" by Laoudi, J., Tate, C. and Voss, C.R.. (C#)
    - Optimized a route-planning application utilizing Simulated Annealing in 3D. ([Planneal][18]; C#)
    - Tested and developed VGIS (Virtual Geographic Information System) in .NET. (C#)

#### Virginia Tech, Blacksburg, Virginia -- Graduate Teaching Assistant

***September 2000 – May 2002***

- Taught undergraduate UNIX course and lab sections.
- Aided students enrolled in Data Structures and File Processing.
- Aided Students enrolled in Object-Oriented Programming.

#### Appropriate Technologies, Inc., Chesapeake, Virginia -- Programming/Debugging Intern

***Summer 1999***  

- Debugged and programmed applications using Visual C++ and SQL database servers.

#### Newport News Public School System, Newport News, Virginia -- Volunteer

***Summer 1998***
- Assisted with database management and development.
- Assisted the Systems Administrator.
- Provided technical support for the school system.

#### URLabs, Hampton, Virginia -- Internet Information Analyst

***Winter 1997-1998***

- Helped maintain and gather information on restricted Internet sites for database data entry.

#### Virginia Tech, Blacksburg, Virginia -- Engineering Fundamentals Facilitator

***Fall 1997***

- Tutored students enrolled in Engineering Fundamentals.

------------------

## EDUCATION

#### Rochester Institute of Technology, Rochester, New York -- Advanced Graduate Certificate, Cybersecurity, 2019

#### Virginia Tech, Blacksburg, Virginia -- M.S., Computer Science and Applications, 2002

- M.S. Project and Report: "[Flexible Design of Interpreters][15]," May 2002

#### Virginia Tech, Blacksburg, Virginia -- B.S., Computer Science, 2000

------------------

## CERTIFICATIONS

#### Red Hat
- [Red Hat Certified System Administrator][45]

#### EDx
- [MicroMasters in Cybersecurity][51] in collaboration with [Rochester Institute of Technology](https://www.rit.edu/).
  - [Cybersecurity Capstone][50]
  - [Computer Forensics][46]
  - [Cybersecurity Risk Management][47]
  - [Network Security][48]
  - [Cybersecurity Fundamentals][49]

------------------

## PUBLICATIONS

- Voss. C.R, et al. "[Boosting Performance of Weak MT Engines Automatically: Using MT Output to Align Segments & Build Statistical Post Editors][1]," Proceedings of the 12th European Association for Machine Translation, September 2008.

[1]: http://mt-archive.info/EAMT-2008-Voss.pdf
[2]: http://www.mt-archive.info/LREC-2006-Laoudi.pdf
[3]: http://www.ns-cta.org/
[4]: http://www.tuio.org/
[5]: http://www.arl.army.mil/
[6]: http://snapcraft.io/
[7]: https://github.com/snapcore/snapcraft/commits?author=ZenHarbinger
[8]: https://github.com/snapcore/snapcraft/releases/tag/2.24
[9]: https://github.com/snapcore/snapcraft/releases/tag/2.25
[10]: {{site.url}}/torgo/
[11]: {{site.url}}/l2fprod-properties-editor/
[12]: https://commons.apache.org/proper/commons-lang/
[13]: https://github.com/apache/commons-lang/pull/88
[14]: https://github.com/ZenHarbinger/spotipy
[15]: {{site.url}}/Interpreter.pdf
[16]: https://kepler-project.org/
[17]: https://mq.java.net/
[18]: http://artistech.com/planneal.html
[19]: http://artistech.com/algolink.html
[20]: http://artistech.com/tie.html
[21]: http://artistech.com/CNF.html
[22]: https://github.com/artistech-inc/VBS3ZeroMqPosPlugin
[23]: https://github.com/artistech-inc/tuio-zeromq-publish
[24]: https://d3js.org/
[25]: http://artistech.com/dare.html
[26]: http://zeromq.org/
[27]: https://developers.google.com/protocol-buffers/
[28]: https://bisimulations.com/virtual-battlespace-3
[29]: http://www.darpa.mil/news-events/2016-03-11
[30]: http://tuio.org/
[31]: https://github.com/mkalten/TUIO11_JAVA/pull/2
[32]: https://github.com/snapcore/snapcraft/releases/tag/2.26
[33]:  {{site.url}}/torgo-javascript/
[34]: https://github.com/adjacentlink/emane
[35]: https://github.com/artistech-inc/social-media-scraper
[36]: https://github.com/artistech-inc/pipeline-base
[37]: https://github.com/artistech-inc/yellow-pipeline-web
[38]: https://github.com/artistech-inc/blue-pipeline-web
[39]: https://github.com/artistech-inc/orange-pipeline-web
[40]: https://github.com/artistech-inc/green-pipeline-web
[41]: https://www.ldap-account-manager.org/lamcms/
[42]: https://ltb-project.org/documentation/self-service-password
[43]: https://www.splunk.com/
[44]: https://puppet.com/
[45]: https://www.redhat.com/rhtapps/certification/verify/?certId=190-011-619
[46]: https://courses.edx.org/certificates/5b8e43fccc9a4c80af61c76d9004b411
[47]: https://courses.edx.org/certificates/6889e9f5766846a2a0fc3f4b582fbd6a
[48]: https://courses.edx.org/certificates/9aead1ab4fec4952b09cfb885727342d
[49]: https://courses.edx.org/certificates/55f75afe391b480d85208f55f0a9760b
[50]: https://courses.edx.org/certificates/e9b436c1f70f43d091f0770214a1265d
[51]: https://credentials.edx.org/credentials/7a0cc89b6a9d4aa388846c7b9c8c03a7/
[52]: https://github.com/dtanner/grails-jasypt/pull/10
[53]: https://plugins.grails.org/plugin/dtanner/org.grails.plugins:jasypt-encryption
[54]: https://github.com/ltb-project/self-service-password/pull/428#issuecomment-711356497]
[55]: https://web.dev/sign-in-form-best-practices/#new-password
[56]: https://github.com/ltb-project/self-service-password/pull/428
[57]: https://web.dev/sign-in-form-best-practices/#new-password
