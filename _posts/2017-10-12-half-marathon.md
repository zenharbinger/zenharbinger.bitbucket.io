---
layout: post
title: Half Marathon
date: '2017-10-12 12:07:17 -0500'
categories: running
tags: [running, half marathon]
author: Matthew Aguirre
---

Second running of the [Prince William Half Marathon](http://www.princewilliamhalf.com/).

[Well, I finished again!](https://runsignup.com/Race/Results/29204/FinishersCert?resultSetId=95806&resultId=19382986#finishersCert)

Finish Time: 2:15:15
![](images/73593245-1389-2017PrinceWilliamHalfGallery2.jpg)  
![](images/73593246-1388-2017PrinceWilliamHalfGallery2.jpg)  

Stats:
![](images/2017-certificate.png)  
