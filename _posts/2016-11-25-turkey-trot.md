---
layout: post
title: 2016 Turkey Trot
date: '2016-11-25 10:20:17 -0400'
categories: running
tags: [running, turkey trot]
author: Matthew Aguirre
---

Ran the [2016 Prince William 5k Turkey Trot yesterday](https://www.flickr.com/photos/125932365@N02/albums/72157676956633615).  

Hit 25:56; 39 seconds slower that last year, but considering health and post-marathon reduction in training, not bad!  

This race was then followed by the [1 mile fun run](https://www.flickr.com/photos/125932365@N02/albums/72157676956633565) which had Riley and Mac running too.  

Mac hit 10 minute mile, Riley hit 11.  Whew!  

Family Photo:  
![](images/family-run.jpg)  

1 mile start  
![](images/15230566_1250309771679233_5555968567069540931_n.jpg)  

1 mile start  
![](images/15193425_1250309478345929_6490216587992774063_n.jpg)  

1 mile start  
![](images/15128859_1250327331677477_6394445528430758992_o.jpg)  
